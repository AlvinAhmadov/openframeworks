#About exampleName

![Screenshot of Example, stored as exampleName/exampleName.png (or .gif or .jpg)](http://i.imgur.com/kGBXk69.jpg)

--

###Learning Objectives

This openFrameworks Example is designed to demonstrate:

* how to ...
* the ability of openFrameworks to ...

*{Guide the reader to what's important in the code of the App.}*


### Expected Behavior

When launching this app, you should see a screen with...

* thing on the screen 1
* thing on the screen 2

Instructions for use:

* Try moving your cursor left-right in order to....
* Press `space` to clear...

*{If there are a lot of key commands, it's not necessarily appropriate here to give detailed instructions about what every keypress does. The In-app Overlays should accomplish that. It may be enough to say something like, "Keypresses allow the user to select among various different effects."}*

###Other classes used in this file

This Example uses the following classes: 

* OtherClass1
* OtherClass2
